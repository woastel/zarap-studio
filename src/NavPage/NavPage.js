import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { Container, Row, Col} from 'react-bootstrap';

import { AgenturPage } from '../AgenturPage';
import { ContactPage } from '../ContactPage';
import { WertePage } from '../WertePage';
import { ProjektePage } from '../ProjektePage';

import './NavPage.css'

const routes = [
  { path: '/agentur', name: '🔥🦄Agentur', Component: AgenturPage },
  { path: '/werte', name: 'Werte', Component: WertePage },
  { path: '/projekte', name: 'Projekte', Component: ProjektePage },
  { path: '/contact', name: 'Kontakt', Component: ContactPage },
]

class HoverLabel extends React.Component {
  // state = { hovered: false };

  constructor(props) {
    super(props);

    // This binding is necessary to make `this` work in the callback
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
  }

  handleMouseEnter() {
    console.log('this is:', this);
  }

  handleMouseLeave() {
    console.log('this is:', this);
  }

  render() {
      return (
          <div onMouseEnter={this.handleMouseEnter}
               onMouseLeave={this.handleMouseLeave}
          >
            {this.props.name}{this.props.children}
          </div>
      );
  }
}


class NavListItem extends React.Component {
  render () {
    return (
      <li>
      <Link
       to={this.props.path}>
      {this.props.value}
      </Link></li>

      
    )
  }
}

class NavList extends React.Component {
  constructor(props){
    super(props);
    this.liste = this.props.list
    this.listitem = this.liste.map((rr, num) =>
      // Correct! Key should be specified inside the array.
      <NavListItem key={num.toString()}
                   value={rr.name} 
                   path={rr.path}/>

    );
    
  }
  render () {
    return (
      <ul> {this.listitem} </ul>
    )
  }
}

class NavListBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: this.props.list,
    }
  }
  
  render () {
    return (
      <Col style={{backgroundColor: "blue"}}>
        <NavList list={this.state.list}/>
      </Col>
    )
  }
}


export class NavPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item_list: routes,
    }
  }

  render() {
    return (    
      <Row style={{backgroundColor: "red"}}>
          <NavListBox list={this.state.item_list}/>
      </Row>
    )
  }
}




export default NavPage
