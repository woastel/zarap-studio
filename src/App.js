import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import Fade from 'react-reveal/Fade';
import withReveal from 'react-reveal/withReveal';
import { BrowserRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';
import { Container, Row, Col } from 'react-bootstrap';
import { HomePage } from './HomePage';
import { NavPage } from './NavPage';
import { AgenturPage } from './AgenturPage';
import { ContactPage } from './ContactPage';
import { WertePage } from './WertePage';
import { ProjektePage } from './ProjektePage';
import { HeadLine } from './HeadLine';

import './App.css';
import 'bootstrap\\dist\\css\\bootstrap.min.css';

const routes2 = [
  { path: '/agentur', name: '🔥🦄Agentur', Component: AgenturPage },
  { path: '/werte', name: 'Werte', Component: WertePage },
  { path: '/projekte', name: 'Projekte', Component: ProjektePage },
  { path: '/contact', name: 'Kontakt', Component: ContactPage },
]


const routes = [
  { path: '/', name: 'Home', Component: HomePage },
  { path: '/navpage', name: 'Navi', Component: NavPage },
  { path: '/agentur', name: 'Agentur', Component: AgenturPage },
  { path: '/werte', name: 'Werte', Component: WertePage },
  { path: '/projekte', name: 'Projekte', Component: ProjektePage },
  { path: '/contact', name: 'Kontakt', Component: ContactPage },
]

function App() {
  return (
    <Router>
      <>
        <div style={{padding: "40px"}}> </div>
        <Container className="container">
          
          {routes.map(({ path, Component }) => (
            <Route key={path} exact path={path}>
              {({ match }) => (
                <CSSTransition
                  in={match != null}
                  timeout={300}
                  classNames="page"
                  unmountOnExit
                >
                  <div className="page">
                    <Component />
                  </div>
                </CSSTransition>
              )}
            </Route>
          ))}


        </Container>
        <HeadLine />
      </>
    </Router>
  )
}


export default App;
