import React, { Component } from 'react'
import makeCarousel from 'react-reveal/makeCarousel';
// we'll need the Slide component for sliding animations
// but you can use any other effect
import Slide from 'react-reveal/Slide';
// we'll use styled components for this tutorial
// but you can use any other styling options ( like plain old css )
import styled, { css } from 'styled-components';

const Container = styled.div`
  position: relative;
  overflow: hidden;
  width: 300px;
  height: 150px;
`;
const CarouselUI = ({ children }) => <Container>{children}</Container>;
const Carousel = makeCarousel(CarouselUI);


function SlogenBox() {
  return (
    <Carousel defaultWait={2000} /*wait for 1000 milliseconds*/ >
      <Slide left>
        <div>
          <h1>Slide 1</h1>
          <p>Slide Description</p>
        </div>
      </Slide>
      <Slide left>
        <div>
          <h1>Slide 2</h1>
          <p>Slide Description</p>
        </div>
      </Slide>
    </Carousel>
  )
}



export class HomePage extends Component {
  render() {
    return (
      <div>
        <h1>Wir machen GOOD Stuff!</h1>
        {/* <SlogenBox /> */}
      </div>
      
    )
  }
}


export default HomePage
