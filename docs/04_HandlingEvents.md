# Endling Events

**Tabel of Contents**

- [Endling Events](#Endling-Events)
  - [Unterschied HTML und React](#Unterschied-HTML-und-React)
  - [Funktion Component based Event Handling](#Funktion-Component-based-Event-Handling)
  - [Class Component based Event Handling](#Class-Component-based-Event-Handling)

Das Arbeiten mit Events in React ist wie in DOM.

* React events are named using camelCase, rather than lowercase.
* With JSX you pass a function as the event handler, rather than a string.

## Unterschied HTML und React

In HTML sieht das aufrufen einer JavaScript Funktion wie follgt aus:

```html
<button onclick="activateLasers()">
  Activate Lasers
</button>
```

Und so schaut das  ganze in React.js aus.

```html
<button onClick={activateLasers}>
  Activate Lasers
</button>
```

## Funktion Component based Event Handling

Sample:

```javascript
function ActionLink() {
  function handleClick(e) {
    e.preventDefault();
    console.log('The link was clicked.');
  }

  return (
    <a href="#" onClick={handleClick}>
      Click me
    </a>
  );
}
```

So würde das ganze in HTML aussehen: 

```html
<a href="#" onclick="console.log('The link was clicked.'); return false">
  Click me
</a>
```

## Class Component based Event Handling
Wenn wir ein `react component` als Klasse erstellen (nach dem ES6 class Standard). Dann ist die Methode die wir an `onClick={}` übergeben eine funktion der Klasse.

```javascript
class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: true};

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }

  render() {
    return (
      <button onClick={this.handleClick}>
        {this.state.isToggleOn ? 'ON' : 'OFF'}
      </button>
    );
  }
}

ReactDOM.render(
  <Toggle />,
  document.getElementById('root')
);
```


You have to be careful about the meaning of `this` in JSX callbacks. In JavaScript, class methods are not [bound](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_objects/Function/bind) by default. If you forget to bind `this`.handleClick and pass it to `onClick`, `this` will be `undefined` when the function is actually called.

This is not React-specific behavior; it is a part of how functions work in JavaScript. Generally, if you refer to a method without `()` after it, such as `onClick={this.handleClick}`, you should bind that method.