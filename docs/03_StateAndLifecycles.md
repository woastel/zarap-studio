# State and Lifecycles - from React Components
React Components haben nicht nur `probs` sondern auch `stats`.  

* props werden als parameter and eine klasse oder funktion übbergeben 
* stats sind die attribute einer klasse

> INFO: deswegen gibt es stats nur in __React.Component__ Klassen.


## Schritt 1: erstellen einer Component Klasse

Eine Component Klasse erstellen. Und eine `render()` Funktion hinzufügen die eine HTML ~~template~~ oder so zurückgibt. Natürlich über die `return()` Funktion.

```javascript
class Clock extends React.Component {
  render() {
    return (
      <div>

      </div>
    );
  }
}
```

Jetzt noch einen Konstruktor hinzufügen der sich um die `props` und `stats` kümmert.

```javascript
    ...
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }
    ...
```

Fertig schaut das ganze dann so aus.

```javascript 
class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }

  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }
}

ReactDOM.render(
  <Clock />,
  document.getElementById('root')
);
```

## Schritt 2: Die Lifecycle Funktionen
Diese Funktionen sind auch bzgl. der ressourcen schonung wichtig.

Wenn ein `Component` erstellt wird. Nennen wir das **mounting**.  
Wenn ein `Component` entfernt wird. Nennen wir das **unmounting**.  

Dem entsprechend gibt es die zwei follgenden Funktionen.

```javascript 
componentDidMount() {
}
```

Wird nach dem Rendern aufgerufen. ? Danach oder doch Davor ?

```javascript 
componentWillUnmount() {
}
```

Wird nach dem Löschen aufgerufen. 


## Schritt3: Wie benutzt man State richtig ?
Laut dem `react.js` Tutorial soll man drei Dingen beim verwenden von `states` beachten. 

1. Do Not Modify State Directly.
2. State Updates may be Asynchronous.
3. State Updates are Merged.

### States nicht direkt bearbeiten.
Nicht `this.state` verwenden außer du es ist im Konstruktor. Dafür gibt es die Funktion `setState()`. 

```javascript 
// falsch
this.state.comment = 'Hello';

// so wirds gemacht
this.setState({comment: 'Hello'});
```

### Beachte Asynchron.

```javascript
// Falsch
this.setState({
  counter: this.state.counter + this.props.increment,
});

// Richtig
this.setState((state, props) => ({
  counter: state.counter + props.increment
}));

// Richtig ohne arrow function
this.setState(function(state, props) {
  return {
    counter: state.counter + props.increment
  };
});
```


### Updates zusammenführen.

```javascript 
componentDidMount() {
    fetchPosts().then(response => {
        this.setState({
        posts: response.posts
        });
    });

    fetchComments().then(response => {
        this.setState({
        comments: response.comments
        });
    });
}
```


## setState mit Funktion oder Object
Es ist besser ein state mit einer `function` upzudaten als mit einem `object`. 

```javascript
// object based update procedure
this.setState({name: !this.state.name});
```
```javascript
// function based update procedure
this.setState((prevState, props) => {
  return {name: !prevState.name});
} 
```