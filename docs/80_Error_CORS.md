# Error: CORS
Es ist follgender Fehler aufgetreten.

- [Error: CORS](#Error-CORS)
  - [Fehlermeldung:](#Fehlermeldung)
  - [Lösung:](#L%C3%B6sung)
  - [Informationen:](#Informationen)

## Fehlermeldung:
![errorScreenshot](images/80_error1.png)

```
Access to fetch at 'http://127.0.0.1:8000/m/members/' from origin 'http://localhost:3000' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. If an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled.
```


## Lösung:
Die Lösung war ganz einfach da ich das Django Framework als Backend Api verwende. Hier muss man nur ein django-package installieren und in den Settings aktiviren.


Das Package heißt `django-cors-headers` und hier gehts zur [GitHub Repo](https://github.com/ottoyiu/django-cors-headers). 


![SetUpDjangoCorsHeaders](images/80_info1.png)
*[pypi.org](https://pypi.org/project/django-cors-headers/) / `2019.07.18`.*


## Informationen:
Sind ein paar Links zu dem Thema:

Wiki: https://de.wikipedia.org/wiki/Cross-Origin_Resource_Sharing.  
Mozilla: https://developer.mozilla.org/de/docs/Web/HTTP/CORS.  

Hier ist auch noch eine super solution.

natürlich wieder auf Stack Overflow zu finden: [link_is_to_long](https://stackoverflow.com/questions/43871637/no-access-control-allow-origin-header-is-present-on-the-requested-resource-whe/43881141)