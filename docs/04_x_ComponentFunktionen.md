# React Component Methoden
Wie rufe ich eine Methode einer `React.Component` Klasse auf. 
Befor wir eine Funktion einer `react.component` (ES6) Klasse aufrufen können müssen wir die Funktion noch mit der Klasse verbinden. 

Dazu gibt es 4 verschieden Möglichkeiten:

1. Bind in der render Funktion
2. Mit arrow function in der render Funktion
3. Bind im Konstruktor
4. Mit einer arrow fuction (dieser weg ist nicht offizel)

## Hier der sichere Weg.
```javascript
class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: true};

    // This binding is necessary to make `this` work in the callback
    this.callFunction = this.callFunction.bind(this);
  }

  callFunction() {
    
  }

  render() {
    return (
      <button onClick={this.callFunction}> Button </button>
    );
  }
}
```

## Hier der Prototyp Weg.  
Hier wird die `arrow_function` verwendet.

```javascript
class LoggingButton extends React.Component {
  // This syntax ensures `this` is bound within handleClick.
  // Warning: this is *experimental* syntax.
  handleClick = () => {
    console.log('this is:', this);
  }

  render() {
    return (
      <button onClick={this.handleClick}>
        Click me
      </button>
    );
  }
}
```

## Oder dieser Weg
```javascript
class LoggingButton extends React.Component {
  handleClick() {
    console.log('this is:', this);
  }

  render() {
    // This syntax ensures `this` is bound within handleClick
    return (
      <button onClick={(e) => this.handleClick.bind(this)}>
        Click me
      </button>
    );
  }
}
```

## Oder dieser Weg
```javascript
class LoggingButton extends React.Component {
  handleClick() {
    console.log('this is:', this);
  }

  render() {
    // This syntax ensures `this` is bound within handleClick
    return (
      <button onClick={(e) => this.handleClick(e)}>
        Click me
      </button>
    );
  }
}
```
