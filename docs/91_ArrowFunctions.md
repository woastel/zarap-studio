# Arrow Functions ( => is good :)


**Tabel of Content**


- [Arrow Functions ( => is good :)](#Arrow-Functions---is-good)
  - [Arrow Funktion syntax](#Arrow-Funktion-syntax)
    - [Funktion with `default` parameter.](#Funktion-with-default-parameter)
    - [Arrow Funktion without parameter](#Arrow-Funktion-without-parameter)
  - [Speichert Funktion oder auch Variable](#Speichert-Funktion-oder-auch-Variable)
  - [Gute Links](#Gute-Links)
  - [Weitere Beispiele](#Weitere-Beispiele)


Die `arrow functions` schauen nur auf den ersten blick verwirrent aus. Aber hat man die Syntax erstmal verstanden visualisieren die sogn. Pfeilfunktionen komplexe Syntax auf eine charmante art und weise.


## Arrow Funktion syntax
Die Syntax der Arrow Functions ist nicht schwer. Hier eine [Seite](https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Functions/Pfeilfunktionen) dazu von Mozilla.

### Funktion with `default` parameter.


### Arrow Funktion without parameter


## Speichert Funktion oder auch Variable


## Gute Links


## Weitere Beispiele
Was sind Arrow Functions ? Ein kleine Codebeispiel: 

```javascript
// Correct
this.setState((state, props) => ({
  counter: state.counter + props.increment
}));
```

Der normale JavaScript code würde so aussehen.

```javascript
// Correct
this.setState(function(state, props) {
  return {
    counter: state.counter + props.increment
  };
});
```