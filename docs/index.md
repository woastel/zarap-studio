![react_screenshot](images/index.png)
*pic from [react.js](https://reactjs.org/)
# Dokumentation nicht fertig also Notes xD xD


- [Dokumentation nicht fertig also Notes xD xD](#Dokumentation-nicht-fertig-also-Notes-xD-xD)
  - [Komm zu Potte! -> Hier Start!](#Komm-zu-Potte---Hier-Start)
  - [Mein Lernfaden durch React.js](#Mein-Lernfaden-durch-Reactjs)
  - [FAQ?](#FAQ)
  - [Weitere Informations about this Notes](#Weitere-Informations-about-this-Notes)
  - [Keywords](#Keywords)

Hier sind nur ein paar Notizen die ich während des erstelles dieser Application gemacht habe.  
Das ist meine erste React.js Application, die ich anhand unterschiedlicher Tutorials erstellt habe. Deswegen werden hier hauptsächlich Notizen zu finden sein die Redundant zu vielen Tutorials sind oder deren Inhalte wiederspieglen.


## Komm zu Potte! ->  [Hier Start!](./00_intro.md) 

Wenns auch gemütlich geht!  

Ich habe die Files in unterschiedliche Kapitel eingeteilt. Es gibt Notes zu dem React Tutorial es gibt aber auch Notes zu Feherbildern die ich während der Entwicklung dieser App hatte. Oder allgehmeine Tutorials wie ein Artikel über `arrow functions`.

## Mein Lernfaden durch React.js
Eine Tabelle mit allen Note Pages.

| **Notes Pages**                                  | note:  |
| :----------------------------------------------- | :----- |
| **Tutorials**                                    |        |
| [Components](02_ComponentsAndProps.md)           |        |
| [State And Lifecycles](03_StateAndLifecycles.md) |        |
| [Handling Event](04_HandlingEvents.md)           |        |
| **Errors**                                       |        |
| [Err: CORS](80_Error_CORS.md)                    | solved |
| **Other Stuff**                                  |        |
| [Arrow Functions](91_ArrowFunctions.md)          |        |


>Die Notes starten mit dem offiziellen [React.js Tutorial](https://reactjs.org/docs/hello-world.html). Eigentlich starten die Notes gleich bei [Components and Props](https://reactjs.org/docs/components-and-props.html).


## FAQ?

**Q: Was bedeutet eine Komponente nach ES6 class zu definieren?**
> Es gibt react `componentes` in zwei verschiedenen arten. Einmal als Funktion und einmal als Klasse. Java Script an sich ist keine OOP Programiersprache. Deswegen gibt es keine Klassen. Um dieses Problem zu lösen wurde der ES6 standard eingeführt, mit dem es möglich ist ein Klasse zu erstellen.

**Q: Was ist JSX?**
> JSX ist eine Template Sprache oder sowas ... diese frage muss ich noch bbeantworten.


## Weitere Informations about this Notes

Das ist das Youtube Viedeo das ich mir angeschaut habe.

https://www.youtube.com/watch?v=sBws8MSXN7A

AJAX vs. Fetch API  

* [medium artikel](https://medium.com/@armando_amador/how-to-make-http-requests-using-fetch-api-and-promises-b0ca7370a444)  
* [heise.de intro fetch](https://www.heise.de/developer/artikel/AJAX-aber-einfacher-Die-neue-Fetch-API-2642427.html)  


ECMAScript6 - 2015  

* [wikipedia ecmascript](https://en.wikipedia.org/wiki/ECMAScript)  
* [w3 schools](https://www.w3schools.com/js/js_es6.asp)  


## Keywords

* CORS django
  * damit eine app unter einer anderen domain auf die rest api zugreifen kann.